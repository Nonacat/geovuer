# GeoVuer

Electron application for visualizing GPS data from `XLS` files. Accepts only XLS files where data starts from 3rd row and has exact structure as below table.

| Number               | Date & time         | Coordinates        | Speed               | Direction                       | Location type |
|----------------------|---------------------|--------------------|---------------------|---------------------------------|---------------|
| $`n \in \mathbb{N}`$ | YYYY-mm-dd hh:MM:ss | Longitude/Latitude | $`k \in \mathbb{N}`$| $`m \in \{0, 1, \ldots, 359\}`$ | satellite     |

Repository contains file `DemoLocationData.xls` containing sample data.

Currently app's UI supports only Polish language.


## Preview

### Map
![Map preview](./MAP_PREVIEW.png)

### Table with data
![Table preview](./TABLE_PREVIEW.png)


## Libraries
- [ElectronJS](https://www.electronjs.org)
- [VueJS 2.x](https://vuejs.org)
- [Vuetify](https://vuetifyjs.com/en)
- [Leaflet](https://leafletjs.com)
- [Leaflet-RoatatedMarker](https://github.com/bbecquet/Leaflet.RotatedMarker)
- [Vue2-Leaflet](https://github.com/vue-leaflet/Vue2Leaflet)
- [Vue2-Leaflet-RotatedMarker](https://github.com/mudin/vue2-leaflet-rotatedmarker)
- [SheetJS](https://github.com/SheetJS/sheetjs)


## Developing
```sh
git clone https://gitlab.com/Nonacat/geovuer
cd geovuer

npm install
npm run electron:serve
```


## Building on Linux

### For Linux (ZIP and AppImage)
```sh
git clone https://gitlab.com/Nonacat/geovuer
cd geovuer

npm install
npm run electron:build-linux
```

### For Windows (ZIP)
Requires `Docker` installed.
```sh
git clone https://gitlab.com/Nonacat/geovuer
cd geovuer

docker run --rm -ti \
    --env ELECTRON_CACHE="/root/.cache/electron" \
    --env ELECTRON_BUILDER_CACHE="/root/.cache/electron-builder" \
    -v ${PWD}:/project \
    -v ${PWD##*/}-node-modules:/project/node_modules \
    -v ~/.cache/electron:/root/.cache/electron \
    -v ~/.cache/electron-builder:/root/.cache/electron-builder \
    electronuserland/builder:wine

npm install
npm run electron:build-windows
```

