import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

import MapView from '@/views/MapView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> =
[
  {
    path: '/',
    name: 'Home',
    component: MapView
  },
  {
    path: '/load',
    name: 'Load',
    component: () => import('@/views/LoadView.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import('@/views/TableView.vue')
  }
]

const router = new VueRouter(
{
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
