const xlsx = require('xlsx');


export var handleOpenAndParseXLS = (event: any, arg: string) =>
{
  let file = xlsx.readFile(arg);

  let sheet = file.Sheets[file.SheetNames[0]];
  var range = xlsx.utils.decode_range(sheet['!ref']);

  let dataToSend = [];
  let dataCount = range.e.r - 1;

  for(var rowNum = 2; rowNum <= range.e.r; rowNum++)
  {
    // Date and time
    var cell = xlsx.utils.encode_cell({r: rowNum, c: 1});
    var datetimeMiliseconds = Date.parse(sheet[cell].w);
    var datetime = new Date(datetimeMiliseconds);
    
    // Latitude/longitude
    var cell = xlsx.utils.encode_cell({r: rowNum, c: 2});
    var link = sheet[cell].f;

    var coordiantes = (/.*".*".*"(.*)\/(.*)".*/s).exec(link);
    var longitude, latitude;
    if(coordiantes && coordiantes.length == 3)
    {
      longitude = coordiantes[1];
      latitude = coordiantes[2];
    }

    // Speed
    var cell = xlsx.utils.encode_cell({r: rowNum, c: 3});
    var speed = parseInt(sheet[cell].w);

    // Direction
    var cell = xlsx.utils.encode_cell({r: rowNum, c: 4});
    var direction = parseInt(sheet[cell].w);

    // Location type
    var cell = xlsx.utils.encode_cell({r: rowNum, c: 5});
    var locationType = sheet[cell].w;

    var rowData = [datetime, latitude, longitude, speed, direction, locationType];
    dataToSend.push(rowData);
    event.reply('load-file-progress-reply',
    {
      read: rowNum - 1,
      total: dataCount
    });
  }

  event.reply('load-file-data-reply', dataToSend);
}