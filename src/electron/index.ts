import { ipcMain } from 'electron';
import { handleOpenAndParseXLS } from './handlers';


function main()
{
  ipcMain.on('load-file', handleOpenAndParseXLS);
}


export default main;