import { ipcRenderer, contextBridge } from 'electron';


contextBridge.exposeInMainWorld('API',
{
    send: (channel: string, data: any) =>
    {
        const validChannels = ['load-file'];
        if(validChannels.includes(channel))
        {
            ipcRenderer.send(channel, data);
        }
    },

    receive: (channel: string, func: any) =>
    {
        const validChannels =
        [
            'load-file-progress-reply', 'load-file-data-reply'
        ];
        if(validChannels.includes(channel))
        {
            ipcRenderer.on(channel, (event, ...args) => func(...args));
        }
    }
});