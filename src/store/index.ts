import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'

import Types from '@/store/store';

import axios from 'axios';

Vue.use(Vuex)


const addressFromCoordinates =
(
  latitude: string,
  longitude: string,
  format = 'json'
) =>
{
  return(
    'https://nominatim.openstreetmap.org/reverse?' +
    `lat=${latitude}&lon=${longitude}&format=${format}`
  );
};


const isIndexInBound = (getters: any, index: number) =>
{
  const objectsCount: number = getters.getLocationsCount;
  if(index < objectsCount && index >= 0 || objectsCount == 0 && index == -1)
  {
    return true;
  }

  return false;
};


//export default new Vuex.Store(
const store: StoreOptions<Types> =
{
  state:
  {
    locations: [],

    currentLocationIndex: -1,
    addressTimeoutState: null,
    
    travelPath:
    {
      traveledCoordinates: [],
      toTravelCoordinates: []
    },

    backendDataProgress:
    {
      dataRead: 0,
      dataTotal: 0
    }
  },

  mutations:
  {
    ADD_LOCATION_OBJECT(state, locationObject: Record<string, any>)
    {
      locationObject.id = state.locations.length;
      state.locations.push(locationObject);
      
      state.travelPath.toTravelCoordinates.push(
        [locationObject.latitude, locationObject.longitude]
      );
    },


    REMOVE_ALL_LOCATION_OBJECTS(state)
    {
      state.locations = [];
      state.currentLocationIndex = -1;

      state.travelPath.traveledCoordinates = [];
      state.travelPath.toTravelCoordinates = [];
    },


    SET_CURRENT_LOCATION_INDEX(state, index: number)
    {
      state.currentLocationIndex = index;
    },


    SET_OBJECT_ADDRESS(state, payload)
    {
      state.locations[payload.index].approximate_address = payload.address;
    },


    MOVE_TRAVEL_PATH(state, moveForward)
    {
      /*
      Assumes future and past path arrays are non empty

      Example:
        past = [1],    future = [1, 2, 3], moveForward = true
          =>
        past = [1, 2], future = [2, 3]
      */

      if(moveForward)
      {
        // If past array is empty
        if(state.travelPath.traveledCoordinates.length == 0)
        {
          const first = state.travelPath.toTravelCoordinates[0];
          state.travelPath.traveledCoordinates.push(first);
        }

        // Remove first element from future coordinates
        if(state.travelPath.toTravelCoordinates.length > 1)
        {
          state.travelPath.toTravelCoordinates.shift();
        }
        
        // Add first (after removal) element from future to past
        const first = state.travelPath.toTravelCoordinates[0];
        state.travelPath.traveledCoordinates.push(first);
      }

      else
      {
        // Remove last element from future coordinates
        state.travelPath.traveledCoordinates.pop();

        // Add last (after removal) element from past to future
        const length = state.travelPath.traveledCoordinates.length;
        const last = state.travelPath.traveledCoordinates[length - 1];
        state.travelPath.toTravelCoordinates.unshift(last);
      }
    },


    SET_TRAVEL_PATH(state, index)
    {
      const traveled: number[][] = [];
      const toTravel: number[][] = [];

      for(let i = 0; i <= index && index < state.locations.length; i++)
      {
        const latitude = (state.locations[i]).latitude;
        const longitude = (state.locations[i]).longitude;

        traveled.push([parseFloat(latitude), parseFloat(longitude)]);
      }

      for(let i = index; i < state.locations.length; i++)
      {
        const latitude = state.locations[i].latitude;
        const longitude = state.locations[i].longitude;

        toTravel.push([parseFloat(latitude), parseFloat(longitude)]);
      }

      state.travelPath.traveledCoordinates = traveled;
      state.travelPath.toTravelCoordinates = toTravel;
    }
  },


  getters:
  {
    getLocationsCount(state)
    {
      return state.locations.length;
    },


    getCurrentLocationIndex(state)
    {
      return state.currentLocationIndex;
    },


    getTravelPath(state)
    {
      return state.travelPath;
    },


    getCurrentLocationObject(state, getters)
    {
      if(getters.getCurrentLocationIndex != -1)
      {
        return state.locations[state.currentLocationIndex];
      }

      else
      {
        return null;
      }
    },
  },


  actions:
  {
    addLocationObject({commit}, locationObject: any)
    {
      commit('ADD_LOCATION_OBJECT', locationObject);
    },
    

    clearLocations({commit})
    {
      commit('REMOVE_ALL_LOCATION_OBJECTS');
    },


    setCurrentLocationIndex({commit, getters}, index: number)
    {
      if(isIndexInBound(getters, index))
      {
        if(Math.abs(getters.getCurrentLocationIndex - index) > 1)
        {
          commit('SET_TRAVEL_PATH', index);
        }

        commit('SET_CURRENT_LOCATION_INDEX', index);
      }
    },


    fetchLocationAddress({commit}, locationObject)
    {
      const latitude = locationObject.latitude;
      const longitude = locationObject.longitude;

      axios.get(
        addressFromCoordinates(latitude, longitude)
      ).then(response =>
      {
        const payload =
        {
          index: locationObject.id,
          address: response
        }

        commit('SET_OBJECT_ADDRESS', payload);
      });
    },


    setCurrentLocationObject({dispatch}, locationObject)
    {
      dispatch('setCurrentLocationIndex', locationObject.id);
    },


    moveToPrevious({commit, dispatch, getters})
    {
      const currentIndex = getters.getCurrentLocationIndex;
      const previousIndex = currentIndex - 1;

      if(isIndexInBound(getters, previousIndex))
      {
        commit('SET_CURRENT_LOCATION_INDEX', previousIndex);

        const traveled = getters.getTravelPath.traveledCoordinates;
        if(traveled.length > 0)
        {
          commit('MOVE_TRAVEL_PATH', false);
          dispatch('setLocationAddressAfter',
          {
            timeout: 800,
            object: getters.getCurrentLocationObject
          });
        }
      }
    },


    moveToNext({commit, dispatch, getters})
    {
      const currentIndex = getters.getCurrentLocationIndex;
      const nextIndex = currentIndex + 1;

      if(isIndexInBound(getters, nextIndex))
      {
        commit('SET_CURRENT_LOCATION_INDEX', nextIndex);

        const toTravel = getters.getTravelPath.toTravelCoordinates;
        if(toTravel.length > 0)
        {
          commit('MOVE_TRAVEL_PATH', true);
          dispatch('setLocationAddressAfter',
          {
            timeout: 800,
            object: getters.getCurrentLocationObject
          });
        }
      }
    },


    setLocationAddressAfter({state, dispatch}, payload)
    {
      const locationObject: Record<string, any> = payload.object;
      const timeout: number = payload.timeout;

      if(state.addressTimeoutState)
      {
        clearTimeout(state.addressTimeoutState);
      }

      state.addressTimeoutState = setTimeout(() =>
      {
        if(locationObject.approximate_address == null)
        {
          dispatch('fetchLocationAddress', payload.object)
        }
      }, timeout);
    }
  },


  modules:
  {
    
  }
};


export default new Vuex.Store(store);
