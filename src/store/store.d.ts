export default interface Store
{
  locations: Record<string, any>[],

  currentLocationIndex: number,
  addressTimeoutState: ReturnType<typeof setTimeout>|null,
  
  travelPath: Record<string, number[][]>
  backendDataProgress: Record<string, number>
}